module.exports = {
    // 用于Cookie加密
    cookieSecret : 'myblog',
    // 数据库名称
    db : 'blog',
    // 数据库地址
    host : 'localhost',
    // 端口号
    port : 27017
    // auto_reconnect: true
};